(function(){
  const buttonT = document.querySelector('.bars')
  const divButtons = document.querySelector('ul')
  const buttonIcon = document.querySelector('.bars i')
  buttonT.addEventListener('click', function(event){
      divButtons.classList.toggle('hide')
      divButtons.classList.toggle('buttons')

      buttonIcon.classList.toggle('fa-bars')
      buttonIcon.classList.toggle('fa-times')
  })
})();